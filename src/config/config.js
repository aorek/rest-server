// =========================
// Port
// =========================
process.env.PORT = process.env.PORT || 3000;

// =========================
// Environment
// =========================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// =========================
// Token
// =========================
process.env.TOKEN_SEED = process.env.TOKEN_SEED || 'L1l3tIe29ljoO0IWbOc5DsMxtnBf8DZJ';
process.env.TOKEN_EXPIRE = '48h';

// =========================
// Google Client ID
// =========================
process.env.CLIENT_ID = process.env.CLIENT_ID || '787219035490-p7bnt12ubl1t0kfme6ujri4p2b0djf7j.apps.googleusercontent.com';

// =========================
// Database
// =========================
let uriDB = '';
if (process.env.NODE_ENV === 'dev') {
   uriDB = 'mongodb://localhost:27017/cafe';
} else {
   uriDB = process.env.MONGO_URI;
}

process.env.DB_URI = uriDB;
