import express from 'express';
import path from 'path';
import setupRoutes from './routes/routes';

const app = express();

// setup logging
// app.use(morgan('combined', { stream: logger.stream }));

const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(express.static(path.resolve(__dirname, '../public')));

// setup routes
setupRoutes(app);

export default app;
