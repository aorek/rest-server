import winston from 'winston';

export const logger = winston.createLogger({
   transports: [
      //
      // - Write to all logs with level `info` and below to `combined.log`
      // - Write all logs error (and below) to `error.log`.
      //
      new winston.transports.Console({
         level: 'info',
         colorize: true,
         prettyPrint: true,
         timestamp: true
      })
   ]
});

logger.stream({ start: -1 }).on('log', (log) => {
   console.log(log);
});
