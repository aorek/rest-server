import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

let roles = {
   values: ['ADMIN_ROLE', 'USER_ROLE'],
   message: '{VALUE} is not a valid role'
};

let Schema = mongoose.Schema;

const userSchema = new Schema({
   name: {
      type: String,
      required: [true, 'The name is required']
   },
   email: {
      type: String,
      unique: true,
      required: [true, 'The email is required']
   },
   password: {
      type: String,
      required: [true, 'The password is required']
   },
   img: {
      type: String
   },
   role: {
      type: String,
      default: 'USER_ROLE',
      enum: roles
   },
   state: {
      type: Boolean,
      default: true
   },
   google: {
      type: Boolean,
      default: false
   }
});

userSchema.methods.toJSON = function () {
   let user = this;
   let userObject = user.toObject();
   delete userObject.password

   return userObject;
}

userSchema.plugin(uniqueValidator, { message: '{PATH} must be unique' });

export default mongoose.model('User', userSchema);
