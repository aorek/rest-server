import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const Schema = mongoose.Schema;

const categorySchema = new Schema({
   description: {
      type: String,
      unique: true,
      required: [true, 'Description is required']
   },
   user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
   }
});

categorySchema.plugin(uniqueValidator, { message: '{PATH} must be unique' });

export default mongoose.model('Category', categorySchema);
