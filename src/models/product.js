import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const productoSchema = new Schema({
   name: { type: String, required: [true, 'Name is required'] },
   unitPrice: { type: Number, required: [true, 'Unit price is required'] },
   description: { type: String, required: false },
   available: { type: Boolean, required: true, default: true },
   category: { type: Schema.Types.ObjectId, ref: 'Category', required: true },
   user: { type: Schema.Types.ObjectId, ref: 'User' }
});

export default mongoose.model('Product', productoSchema);
