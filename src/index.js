require("./config/config");

import mongoose from 'mongoose';
import 'babel-polyfill';
import app from './app';
// import logger from './utils/logger';

mongoose.connect(process.env.DB_URI, { useCreateIndex: true, useNewUrlParser: true }, (err, res) => {
   if (err) throw err;

   console.log('Database ready, starting server...');

   app.listen(process.env.PORT, () => console.log(`Listening on port ${process.env.PORT}`))
});
