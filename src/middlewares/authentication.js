import jwt from 'jsonwebtoken';

export const tokenVerification = (req, res, next) => {
   const token = req.get('token');

   jwt.verify(token, process.env.TOKEN_SEED, (err, decoded) => {
      if (err) {
         return res.status(401).json({
            ok: false,
            err: {
               message: 'Token invalid!'
            }
         });
      }

      req.user = decoded.user
   });

   next();
}

export const adminVerification = (req, res, next) => {
   const user = req.user;

   if (user.role !== 'ADMIN_ROLE') {
      return res.status(401).json({
         ok: false,
         err: {
            message: 'You have not enought rights'
         }
      });
   }

   next();
}
