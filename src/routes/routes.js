import setupCategoryRoutes from "./category";
import setupLoginRoutes from "./login";
import setupProductRoutes from "./product";
import setupUserRoutes from "./user";

export default (app) => {
   setupCategoryRoutes(app);
   setupLoginRoutes(app);
   setupProductRoutes(app);
   setupUserRoutes(app);
}
