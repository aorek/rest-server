import User from "../../models/user";
import { adminVerification, tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.delete('/user/:id', [tokenVerification, adminVerification], (req, res) => {
      const id = req.params.id;
      const options = {
         new: true,
      }
      User.findByIdAndUpdate(id, { state: false }, options, (err, userDeleted) => {
         if (err) {
            return res.status(400).json({
               ok: false,
               err
            });
         };

         if (!userDeleted) {
            return res.status(400).json({
               ok: false,
               err: {
                  message: 'User not found'
               }
            })
         };

         res.json({
            ok: true,
            user: userDeleted
         });
      });

   });
}
