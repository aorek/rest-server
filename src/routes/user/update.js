import _ from 'underscore';
import User from '../../models/user';
import { adminVerification, tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.put('/user/:id', [tokenVerification, adminVerification], (req, res) => {
      const options = {
         new: true,
         runValidators: true
      }
      let id = req.params.id;
      let body = _.pick(req.body, ['name', 'email', 'password', 'img', 'state']);

      User.findByIdAndUpdate(id, body, options, (err, userDB) => {
         if (err) {
            return res.status(400).json({
               ok: false,
               err
            })
         }

         res.json({
            ok: true,
            user: userDB
         })
      });

   });
}
