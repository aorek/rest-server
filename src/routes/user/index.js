import create from "./create";
import get from "./get";
import update from "./update";
import deleteUser from "./delete";

export default (app) => {
   create(app);
   get(app);
   update(app);
   deleteUser(app);
}
