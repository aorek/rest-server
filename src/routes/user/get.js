import User from '../../models/user';
import { tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.get('/user', tokenVerification, (req, res) => {

      let skip = Number(req.query.skip) || 0;
      let limit = Number(req.query.limit) || 5;

      User.find({ state: true }, 'name email role google')
         .skip(skip)
         .limit(limit)
         .exec((err, users) => {
            if (err) {
               return res.status(400).json({
                  ok: false,
                  err
               })
            }

            User.countDocuments({ state: true }, (err, count) => {
               res.json({
                  ok: true,
                  users,
                  count
               })
            })
         })
   });
}
