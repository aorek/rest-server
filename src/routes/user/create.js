import bcrypt from 'bcrypt-nodejs';
import User from '../../models/user';
import { adminVerification, tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.post('/user', [tokenVerification, adminVerification], (req, res) => {
      let body = req.body;

      let user = new User({
         name: body.name,
         email: body.email,
         password: bcrypt.hashSync(body.password, 10),
         role: body.role
      });

      user.save((err, userDB) => {
         if (err) {
            return res.status(400).json({
               ok: false,
               err
            })
         }

         res.status(201).json({
            ok: true,
            user: userDB
         });
      });
   });
}
