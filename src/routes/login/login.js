import bcrypt from 'bcrypt-nodejs';
import { OAuth2Client } from 'google-auth-library';
import jwt from 'jsonwebtoken';

import User from '../../models/user';

export default (app) => {
   app.post('/login', (req, res) => {
      const body = req.body;

      User.findOne({ email: body.email }, (err, userDB) => {
         if (err) {
            return res.status(500).json({
               ok: false,
               err
            })
         }

         if (!userDB || !bcrypt.compareSync(body.password, userDB.password)) {
            return res.status(400).json({
               ok: false,
               err: {
                  message: 'User or password not match!'
               }
            })
         }

         console.log(userDB);

         let token = jwt.sign({
            user: userDB
         }, process.env.TOKEN_SEED, { expiresIn: process.env.TOKEN_EXPIRE })

         res.json({
            ok: true,
            user: userDB,
            token
         })
      })
   });

   app.post('/google', async (req, res) => {
      const token = req.body.idtoken;

      console.log(token);

      const googleUser = await verify(token)
         .catch(err => {
            return res.status(403).json({
               ok: false,
               err
            });
         });

      User.findOne({ email: googleUser.email }, (err, userDB) => {

         if (err) {
            return res.status(500).json({
               ok: false,
               err
            });
         }

         if (userDB) {
            if (!userDB.google) {
               return res.status(400).json({
                  ok: false,
                  err: {
                     message: 'You must use the user and password you used to sign in'
                  }
               });
            } else {
               let token = jwt.sign({
                  user: userDB
               }, process.env.TOKEN_SEED, { expiresIn: process.env.TOKEN_EXPIRE });

               return res.json({
                  ok: true,
                  user: userDB,
                  token
               });
            }
         } else {
            let user = new User();

            user.name = googleUser.name;
            user.email = googleUser.email;
            user.img = googleUser.img;
            user.google = true;
            user.password = ':)';

            user.save((err, userDB) => {
               if (err) {
                  return res.status(500).json({
                     ok: false,
                     err
                  });
               }

               let token = jwt.sign({
                  user: userDB
               }, process.env.TOKEN_SEED, { expiresIn: process.env.TOKEN_EXPIRE });

               return res.json({
                  ok: true,
                  user: userDB,
                  token
               });
            });
         }
      });

      // res.json({
      //    user: googleUser
      // });
   });
}

async function verify(token) {
   const client = new OAuth2Client(process.env.CLIENT_ID);
   const ticket = await client.verifyIdToken({
      idToken: token,
      audience: process.env.CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
   });
   const payload = ticket.getPayload();

   return {
      name: payload.name,
      email: payload.email,
      img: payload.picture,
      google: true
   }

}
