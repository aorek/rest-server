import _ from 'underscore';

import Product from '../../models/product';
import { tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.put('/product/:id', tokenVerification, (req, res) => {
      let id = req.params.id;
      let body = _.pick(req.body, ['name', 'unitPrice', 'description', 'available', 'category']);

      Product.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, productDB) => {
         if (err) {
            return res.status(400).json({
               ok: false,
               err
            })
         }

         if (!productDB) {
            return res.status(404).json({
               ok: false,
               err: {
                  message: 'Product id not found'
               }
            })
         }

         res.json({
            ok: true,
            product: productDB
         })
      });
   });
}
