import Product from '../../models/product';
import { tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.get('/product', tokenVerification, (req, res) => {
      let skip = Number(req.query.skip) || 0;
      let limit = Number(req.query.limit) || 5;

      Product.find({ available: true })
         .skip(skip)
         .limit(limit)
         .sort('description')
         .populate('user', 'name email')
         .populate('category', 'description')
         .exec((err, productsDB) => {
            if (err) {
               return res.status(500).json({
                  ok: false,
                  err
               })
            }

            res.json({
               ok: true,
               products: productsDB
            });
         });
   });

   app.get('/product/:id', tokenVerification, (req, res) => {
      const id = req.params.id;

      Product.findById(id)
         .sort('description')
         .populate('user', 'name email')
         .populate('category', 'description')
         .exec((err, productDB) => {
            if (err) {
               return res.status(500).json({
                  ok: false,
                  err
               })
            }

            if (!productDB) {
               return res.status(404).json({
                  ok: false,
                  err: {
                     message: 'Product id not found.'
                  }
               })
            }

            res.json({
               ok: true,
               product: productDB
            });
         });
   });

   app.get('/product/find/:term', tokenVerification, (req, res) => {
      const term = req.params.term;
      const regex = new RegExp(term, 'i');


      Product.find({ name: regex })
         .sort('description')
         .populate('user', 'name email')
         .populate('category', 'description')
         .exec((err, productDB) => {
            if (err) {
               return res.status(500).json({
                  ok: false,
                  err
               })
            }

            if (!productDB) {
               return res.status(404).json({
                  ok: false,
                  err: {
                     message: 'Product id not found.'
                  }
               })
            }

            res.json({
               ok: true,
               product: productDB
            });
         });
   });
}
