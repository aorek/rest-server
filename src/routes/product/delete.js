import Product from '../../models/product';
import { tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.delete('/product/:id', tokenVerification, (req, res) => {
      let id = req.params.id;

      Product.findOneAndUpdate({ _id: id }, { available: false }, { new: true, runValidators: true }, (err, productDB) => {
         if (err) {
            return res.status(500).json({
               ok: false,
               err
            })
         }

         if (!productDB) {
            return res.status(404).json({
               ok: false,
               err: {
                  message: 'Product id not found.'
               }
            })
         }

         res.json({
            ok: true,
            product: productDB,
            message: 'Product is unavailable'
         })
      });
   });
}
