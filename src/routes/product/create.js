import _ from 'underscore';

import Product from '../../models/product';
import { tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.post('/product', tokenVerification, (req, res) => {
      const body = _.pick(req.body, ['name', 'unitPrice', 'description', 'available', 'category']);

      const product = new Product({
         name: body.name,
         unitPrice: body.unitPrice,
         description: body.description,
         available: body.available,
         category: body.category,
         user: req.user._id
      });

      product.save((err, productDB) => {
         if (err) {
            return res.status(500).json({
               ok: false,
               err
            })
         }

         if (!productDB) {
            return res.status(400).json({
               ok: false,
               err: {
                  message: 'Product not created'
               }
            })
         }

         res.status(201).json({
            ok: true,
            product: productDB
         });
      });
   });
}
