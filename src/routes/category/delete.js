import Category from '../../models/category';
import { adminVerification, tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.delete('/category/:id', [tokenVerification, adminVerification], (req, res) => {
      const id = req.params.id;

      Category.findOneAndDelete({ _id: id }, (err, categoryDB) => {
         if (err) {
            return res.status(400).json({
               ok: false,
               err
            });
         }

         if (!categoryDB) {
            return res.status(400).json({
               ok: false,
               err: {
                  message: 'Category not found'
               }
            })
         };

         res.json({
            ok: true,
            category: categoryDB
         });
      });
   });
}
