import _ from 'underscore';

import Category from '../../models/category';
import { tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.put('/category/:id', tokenVerification, (req, res) => {
      let id = req.params.id;
      let body = _.pick(req.body, ['description']);

      Category.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, categoryDB) => {
         if (err) {
            return res.status(400).json({
               ok: false,
               err
            })
         }

         if (!categoryDB) {
            return res.status(404).json({
               ok: false,
               err: 'Category id not found'
            })
         }

         res.json({
            ok: true,
            category: categoryDB
         })
      });
   });
}
