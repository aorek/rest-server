import Category from '../../models/category';
import { tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.get('/category', tokenVerification, (req, res) => {
      Category.find({})
         .sort('description')
         .populate('user', 'name email')
         .exec((err, categoriesDB) => {
            if (err) {
               return res.status(400).json({
                  ok: false,
                  err
               })
            }

            res.json({
               ok: true,
               categories: categoriesDB
            });
         });
   });

   app.get('/category/:id', tokenVerification, (req, res) => {
      let id = req.params.id;

      Category.findById(id, (err, categoryDB) => {
         if (err) {
            return res.status(400).json({
               ok: false,
               err
            })
         }

         if (!categoryDB) {
            return res.status(500).json({
               ok: false,
               err: 'The id not exists'
            })
         }

         res.json({
            ok: true,
            category: categoryDB
         });
      });
   });
}
