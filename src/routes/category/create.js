import Category from '../../models/category';
import { tokenVerification } from '../../middlewares/authentication';

export default (app) => {
   app.post('/category', tokenVerification, (req, res) => {
      const body = req.body;

      const category = new Category({
         description: body.description,
         user: req.user._id
      });

      category.save((err, categoryDB) => {
         if (err) {
            return res.status(500).json({
               ok: false,
               err
            })
         }

         if (!categoryDB) {
            return res.status(400).json({
               ok: false,
               err
            })
         }

         res.status(201).json({
            ok: true,
            category: categoryDB
         });
      });
   });
}
